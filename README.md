# Interview Test

## Deploy on localhost
* Download Go language on local computer [download go](https://go.dev/doc/install) and in visual code (open visual code select on the left of the extension then search for Go.
* Clone from [git hub](https://github.com/go-training/helloworld.git)
* command in local  
```
git clone https://github.com/go-training/helloworld.git
```
* Don't forget to create a new folder first for this project
* Open the project in VS Code
* Open the main.go file and enter the following code

```
package main

import (
    "fmt"
    "net/http"
)

func hello(w http.ResponseWriter, req *http.Request) {

    fmt.Fprintf(w, "hello\n")
}

func main() {

    http.HandleFunc("/", hello)

    http.ListenAndServe(":8090", nil)
}
```

* jalankan command di terminal 
```
go run main.go
```
* bopen in local localhost:8090


## Deploy using AWS Free Tier
*Creating EC2 Instances*
1. I'm using *Ubuntu OS, Ubuntu Server 22.04 LTS (HVM), SSD Volume Type, t2.micro for Instance type*
2. Create a key pair
3. Set the network setting. Here I set the security group as a firewall for the Ec2 instance. I created a new security group with: *Allow SSH traffic from* and *Allow HTTP traffic from the internet*
4. Storage configuration. I only use *1x8 GIB gp2* because the file to be deployed is not large
5. I use SSH to run the command
6. Before installing golang I updated the package with the command
```
sudo apt update
```
7. Install go language using command 
```
sudo apt install golang-go
```
8. Then i make a file
```
touch main.go
```
9. To fill it I use the command  
```
vim main.go
```
10. The code I put in the file is as follows
```
package main

import (
    "fmt"
    "net/http"
)

func hello(w http.ResponseWriter, req *http.Request) {

    fmt.Fprintf(w, "hello\n")
}

func main() {

    http.HandleFunc("/", hello)

    http.ListenAndServe(":80", nil)
}
```

11. After that I run using the command  
```
sudo go run main.go
```
12. The results can be accessed via http://18.134.182.162/

## B. Create a system so that it can always run. 
1. I make a file 
```
sudo vim /etc/systemd/system/hello.service
```
then add
```
Description=hello program

[Service]
ExecStart=home/ubuntu/interview-test/main

[Install]
WantedBy=multi-user.target
```
2. once the file is ready. i ran the following commands
```
sudo systemctl daemon-reload
```

```
sudo systemctl enable hello.service 
```

```
sudo systemctl start hello.service
```
## C. Adding a Load Balancer
1. Create Application Load Balancer with conditions
    * Scheme: Internet Facing
	* IP address type: IPv4
	* Network mapping: all AZs
	* Make security group: 
		* Inbound rule: allow http from anywhere
		* Outboun rule: All traffic to anywhere
		* Listener: HTTP:80
2. The results can be accessed using [following link](interview-test-lb-2086915882.eu-west-2.elb.amazonaws.com)

## Git Add, Commit and Push

1. Download git from terminal
2. Cek git using 
```
git version
```
3. login to the specific directory we've created (interview-test.git)
4. cek repository 
```
git status
```
5. If it has not been managed run the following command to create a local repository then it will go to the branch
```
git init
```
6. cek the status
```
git status
```
1. make new repositori di  gitlab
2. Run this command to remote
```
git remote origin 
https://gitlab.com/aj.rumi87/interview-test.git
```
9. check if it can be remote.
```
git remote -v
```
10. I make new directory main.go and README.md using this command
```
mkdir main.go
mkdir README.md
```
11. The files is untracked so i run command bellow to track all files
```
git add -A
```
12. Now the files are ready to commit by using  command bellow
```
git commit -m "menambahkan file main.go dan menambahkan README.md
```
13. Now run push to send what we make in local to remote repository by using command bellow
```
git push origin main
```

